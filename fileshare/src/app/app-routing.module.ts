import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './content/pages/dashboard/dashboard.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },

  { path: 'upload/:id', loadChildren: () => import('./content/pages/upload/upload.module').then(m => m.UploadModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
