import { Action } from '@ngrx/store';

export enum UploadsActionTypes {
    UPLOADSINIT = '[UPLOADS] UPLOADSINIT',
    UPLOADSADD = '[UPLOADS] UPLOADSADD'
}

export class UPLOADSINIT implements Action {
    readonly type = UploadsActionTypes.UPLOADSINIT;
    constructor(public payload: Array<any>) {}
}

export class UPLOADSADD implements Action {
    readonly type = UploadsActionTypes.UPLOADSADD;
    constructor(public payload: any) {}
}


export type Actions =
    | UPLOADSINIT
    | UPLOADSADD;
