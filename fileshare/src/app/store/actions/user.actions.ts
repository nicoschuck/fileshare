import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { User } from 'src/app/services/interfaces/user';

export enum UserActionTypes {
    USERLOGIN = '[User] LOGIN',
    USERADDDATAAZURE = '[User] USERADDDATAAZURE',
    USERLOGOUT = '[User] LOGOUT'
}

export class USERLOGIN implements Action {
    readonly type = UserActionTypes.USERLOGIN;
    constructor(public payload: any) {}
}

export class USERADDDATAAZURE implements Action {
    readonly type = UserActionTypes.USERADDDATAAZURE;
    constructor(public payload: any) {}
}


export class USERLOGOUT implements Action {
    readonly type = UserActionTypes.USERLOGOUT;
}

export type Actions =
    | USERLOGIN
    | USERADDDATAAZURE
    | USERLOGOUT;
