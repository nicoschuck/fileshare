import { Action } from '@ngrx/store';

export enum LanguageActionTypes {
    LANGUAGECHANGE = '[Language] LANGUAGECHANGE'
}

export class LANGUAGECHANGE implements Action {
    readonly type = LanguageActionTypes.LANGUAGECHANGE;
    constructor(public payload: string) {}
}


export type Actions =
    | LANGUAGECHANGE;
