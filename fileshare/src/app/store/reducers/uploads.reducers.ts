import * as UploadsActions from '../actions/uploads.actions';
import {  Uploads, initialUploads } from 'src/app/services/interfaces/uploads';


export function uploadsReducer(state: Uploads = initialUploads, action: UploadsActions.Actions) {
    switch (action.type) {

        case UploadsActions.UploadsActionTypes.UPLOADSINIT:
            const uploads: Uploads = Object.create(initialUploads);
            uploads.files = action.payload;
            uploads.initiated = true;
            return Object.assign({}, state , uploads);

        case UploadsActions.UploadsActionTypes.UPLOADSADD:
            const uploads1: Uploads = Object.create(state);
            uploads1.files.push(action.payload);
            return Object.assign({}, state , uploads1);

        default:
            return state;
    }
}
