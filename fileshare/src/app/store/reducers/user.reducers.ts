import { Action } from '@ngrx/store';
import * as UserActions from '../actions/user.actions';
import { User, initialUser } from 'src/app/services/interfaces/user';


export function userReducer(state: User = initialUser, action: UserActions.Actions) {

    const adminGroupId = '86ea605a-09f9-4b01-8f39-0943f43d2d12';
    const developerGroupId = '700500d3-7901-4b31-b7f9-5aebfe79713c';

    switch (action.type) {

        case UserActions.UserActionTypes.USERLOGIN:
            const user1 = Object.create(initialUser);
            user1.email = action.payload.upn;
            user1.name.firstName = action.payload.given_name;
            user1.name.lastName = action.payload.family_name;
            user1.data.jwt = action.payload;
            user1.online = true;
            return Object.assign({}, state , user1);

        case UserActions.UserActionTypes.USERADDDATAAZURE:
            const user3 = Object.create(state);
            user3.admin = action.payload.value.filter(el => el.objectId === adminGroupId).length > 0;
            user3.developer = action.payload.value.filter(el => el.objectId === developerGroupId).length > 0;
            user3.data.azure = action.payload.value;
            return Object.assign({}, state , user3);

        case UserActions.UserActionTypes.USERLOGOUT:
            return Object.assign({}, state , initialUser);

        default:
            return state;
    }
}
