import * as LanguageActions from '../actions/language.actions';
import { Language, initialLanguage } from 'src/app/services/interfaces/language';



export function lagnuageReducer(state: Language = initialLanguage, action: LanguageActions.Actions) {
    switch (action.type) {

        case LanguageActions.LanguageActionTypes.LANGUAGECHANGE:
            const lang = {
                language: action.payload,
                initiated: true
            };
            return Object.assign({}, state , lang);

        default:
            return state;
    }
}
