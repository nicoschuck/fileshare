import * as language from './reducers/language.reducers';
import { Language } from '../services/interfaces/language';
import * as uploads from './reducers/uploads.reducers';
import { Uploads } from '../services/interfaces/uploads';
import * as user from './reducers/user.reducers';
import { User } from 'src/app/services/interfaces/user';




export interface AppState {
    readonly language: Language;
    readonly uploads: Uploads;
    readonly user: User;
}

export const AppReducers: any = {
    language: language.lagnuageReducer,
    uploads: uploads.uploadsReducer,
    user: user.userReducer
};
