import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './content/static/header/header.component';
import { FooterComponent } from './content/static/footer/footer.component';
import { MessageComponent } from './content/static/message/message.component';
import { MsgComponent } from './content/static/message/msg/msg.component';
import { DashboardComponent } from './content/pages/dashboard/dashboard.component';
import { InfosComponent } from './content/static/header/infos/infos.component';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { AppReducers } from './store/app.states';


import { SharedModule } from './content/dynamic/shared/shared.module';
import { FileUploadModule } from './content/dynamic/file-upload/file-upload.module';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { SideMenuComponent } from './content/static/side-menu/side-menu.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MessageComponent,
    MsgComponent,
    DashboardComponent,
    InfosComponent,
    SideMenuComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,

    HttpClientModule,
    StoreModule.forRoot(AppReducers),

    SharedModule, FileUploadModule,
    MatSnackBarModule, MatDialogModule, MatMenuModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
