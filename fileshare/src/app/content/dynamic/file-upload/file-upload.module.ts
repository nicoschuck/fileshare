import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropDirective } from 'src/app/services/directices/drag-drop.directive';
import { FileUploadComponent } from './file-upload.component';
import { SharedModule } from '../shared/shared.module';







@NgModule({
  declarations: [
    DragDropDirective,
    FileUploadComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [
    DragDropDirective,
    FileUploadComponent
  ]
})
export class FileUploadModule { }
