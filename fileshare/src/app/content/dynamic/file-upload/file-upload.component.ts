import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from '../../static/message/message.service';
import { FileUploadService } from './file-upload.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

  @Input() maxItems = 1;
  @Input() height = 200;
  files: any = [];

  constructor(private messageService: MessageService, private fileUploadService: FileUploadService) { }

  ngOnInit(): void {
  }


  uploadFile(files: FileList) {
    if (files.length + this.files.length > this.maxItems ) {
      if (this.maxItems > 1) {
        this.messageService.sendError('Sie können nur ' + this.maxItems + ' Dateien Hochladen', null);
      } else {
        this.messageService.sendError('Sie können nur eine Datei Hochladen', null);
      }
    } else {
      // const reader = new FileReader();

      const element = event;
      Array.from(files).forEach(file => {
        this.files.push(file);
      });
      console.log(this.files);
      this.fileUploadService.files.emit(this.files);
    }
  }

  deleteAttachment(index) {
    this.files.splice(index, 1);
    this.fileUploadService.files.emit(this.files);
  }
}
