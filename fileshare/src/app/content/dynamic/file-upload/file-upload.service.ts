import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  @Output() files: EventEmitter<Array<File>> = new EventEmitter();

  constructor() { }
}
