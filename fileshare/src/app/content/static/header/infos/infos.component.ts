import { Component, OnInit, Inject } from '@angular/core';
import { TextBlock } from 'src/app/services/interfaces/textblock';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-infos',
  templateUrl: './infos.component.html',
  styleUrls: ['./infos.component.scss']
})
export class InfosComponent implements OnInit {


  public textblocks: Array<TextBlock> = new Array(
    { title: {
      de: 'Wie funktioniert Fileshare?',
      en: 'How does Fileshare work?'
    }, desc: {
      de: 'Sie können ...',
      en: 'You can ...'
    } },
    { title: {
      de: 'Wie groß dürfen die Dateien sein?',
      en: 'How big can the files be?'
    }, desc: {
      de: '...',
      en: '...'
    } }
  );

  constructor(private dialogRef: MatDialogRef<InfosComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    console.log(this.data);
  }

  close() {
    this.dialogRef.close();
  }

}
