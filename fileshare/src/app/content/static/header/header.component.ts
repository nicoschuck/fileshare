import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { InfosComponent } from './infos/infos.component';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.states';
import { User, initialUser } from 'src/app/services/interfaces/user';
import { Language, initialLanguage } from 'src/app/services/interfaces/language';
import { LANGUAGECHANGE } from 'src/app/store/actions/language.actions';
import { MessageService } from '../message/message.service';
import { Languages } from 'src/app/services/interfaces/languages';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public language: Language = initialLanguage;
  private languageMsg: Languages = {
    de: 'Die Sprache wurde auf Deutsch geändert',
    en: 'The language was changed to English'
  };
  public languages: Array<any> = new Array(
    { title: { de: 'Deutsch', en: 'German'} , val: 'de' },
    { title: { de: 'Englisch', en: 'English'} , val: 'en' },
  );
  public uploads: Array<any> = new Array(
    { title: 'Upload 1', desc: 'xxxxx', id: 'nqo3whd3oiwh1qo'},
    { title: 'Upload 2', desc: 'xxxxx', id: 'awwqoq8eqweq4qo'}
  );
  public user: User = initialUser;

  constructor(public dialog: MatDialog, private store: Store<AppState>, private messageService: MessageService) { }

  ngOnInit(): void {
    this.store.select('user').subscribe((state: User) => {
      console.log('user', state);
      this.user = state;
    });

    this.store.select('language').subscribe((state: Language) => {
      console.log('lang', state);
      this.language = state;
    });
  }

  changeLanguage(lang: string): void {
    if (lang !== this.language.language) {
      localStorage.setItem('fileshare-language', lang);
      this.store.dispatch(new LANGUAGECHANGE(lang));
      this.messageService.sendSuccess(this.languageMsg[lang]);
    }
  }

  openInfos() {
    const dialogRef = this.dialog.open(InfosComponent, {
      width: '600px',
      maxWidth: '95%',
      height: '650px',
      maxHeight: '95%',
      autoFocus: false,
      // disableClose: true
      data: { lang: this.language.language }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
