import { Injectable, Output, EventEmitter } from '@angular/core';
import { Message } from 'src/app/services/interfaces/message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  @Output() newMessage: EventEmitter<Message> = new EventEmitter();

  constructor() { }

  public sendError(msg: string, err: any, duration = 0, icon = 'close') {
    console.log(msg);
    this.newMessage.emit({txt: msg, type: 'error', icon, dur: duration});
  }

  public sendInfo(msg: string, duration = 0, icon = 'close') {
    console.log(msg, duration);
    this.newMessage.emit({txt: msg, type: 'info', icon, dur: duration});
  }

  public sendSuccess(msg: string, duration = 0, icon = 'close') {
    console.log(msg);
    this.newMessage.emit({txt: msg, type: 'success', icon, dur: duration});
  }
}
