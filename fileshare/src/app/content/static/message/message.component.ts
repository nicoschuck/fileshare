import { Component, OnInit } from '@angular/core';
import { MessageService } from './message.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Message } from 'src/app/services/interfaces/message';
import { MsgComponent } from './msg/msg.component';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  constructor(private messageService: MessageService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.messageService.newMessage.subscribe((msg: Message) => {
      this.sendMessage(msg.txt, msg.type, msg.icon, msg.dur);
    });
  }

  sendMessage(message: string, type: string, symbole: string, dur: number) {
    this.snackBar.openFromComponent(MsgComponent, {
      data: {
        message,
        icon: symbole
      },
      duration: dur,
      horizontalPosition: 'center',
      panelClass: 'message-' + type
    });
    /*
    this.snackBar.open(message, 'x', {
      duration: 0,
    });*/
  }

}
