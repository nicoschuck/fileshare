import { Component, OnInit } from '@angular/core';
import { Timestamp } from 'rxjs';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  public uploads = new Array(
    { title: 'test1', expire: 1585849708150, link: '', expireIn: 0, expireInDate: '' },
    { title: 'test2', expire: 1585949608150, link: '', expireIn: 0, expireInDate: '' },
    { title: 'test3', expire: 1585879608150, link: '', expireIn: 0, expireInDate: '' }
  );

  constructor() { }

  ngOnInit(): void {

    this.uploads.forEach((el, i) => {
      this.uploads[i].expireIn = this.uploads[i].expire - new Date().getTime() - 1000 * 60;
    });

    console.log(new Date().getTime());
    setInterval(() => {
      console.log(Date.now());
      this.uploads.forEach((el, i) => {
        this.uploads[i].expireIn -= 60000;
      });
    }, 60000);
  }

  getExpireTime(t) {
    console.log(new Date(t));
    return this.toHHMMSS(t / 1000);
  }

  toHHMMSS(t) {
    const sec = parseInt(t, 10); // don't forget the second param
    const dd    = Math.floor(sec / 86400);
    const hours   = Math.floor((sec - (dd * 86400)) / 3600);
    const minutes = Math.floor((sec - (hours * 3600)) / 60);
    // const seconds = sec - (hours * 3600) - (minutes * 60);

    // const hh = (hours   < 10) ? '0' + hours : hours;
    // const mm = (minutes   < 10) ? '0' + minutes : minutes;
    // const ss = (seconds   < 10) ? '0' + seconds : seconds;

    let result = '';
    if (t > 86400) { result = dd + 'd ' + ((hours   < 10) ? '0' + hours : hours) + 'h'; } else {
    if (t > 10800) { result = hours + 'h ' + ((minutes < 10) ? '0' + minutes : minutes) + 'min'; } else {
    if (t > 3600) { result = hours + 'h'; } else {
    if (t > 60) { result = minutes + 'min'; } else {
      result = '>1min';
    }}}}

    return result;
  }
}
