import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FileUploadService } from '../../dynamic/file-upload/file-upload.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  public files: Array<File>;
  private unsub: Subscription = null;

  constructor(private fileUploadService: FileUploadService) { }

  ngOnInit(): void {
    this.unsub = this.fileUploadService.files.subscribe(files => {
      this.files = files;
      console.log(this.files);
    });
  }

  ngOnDestroy(): void {
    this.unsub.unsubscribe();
  }

}
