import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit, OnDestroy {

  public uploadId = null;
  private unsub: Subscription = null;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    // We want to subscribe on route changes
    // Other option is this.route.params.id
    this.unsub = this.route.paramMap.subscribe((params: any) => {
      this.uploadId = params.params.id;
    });
  }

  ngOnDestroy(): void {
    this.unsub.unsubscribe();
  }

}
