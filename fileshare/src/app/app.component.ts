import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from './content/static/message/message.service';
import { environment } from 'src/environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from './store/app.states';
import { USERLOGIN } from './store/actions/user.actions';
import { LANGUAGECHANGE } from './store/actions/language.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  /*
    STATUS
    1 = Start Component
    2 = Start Initialize Logic
    3 = Finish DB requests
    4 = Error
  */
  public status = 1;

  constructor(private appService: AppService, private http: HttpClient, private router: Router,
              private route: ActivatedRoute, private messageService: MessageService, private store: Store<AppState>) {
  }

  ngOnInit(): void {

    this.status = 2;

    const lang = localStorage.getItem('fileshare-language');
    if (lang) {
      this.store.dispatch(new LANGUAGECHANGE(lang));
    }

    // call backend for user data
    this.http.get(environment.apiUrl + 'user').subscribe((response: Array<any>) => {
      console.log(response);
      // DISPATCH USER DATA TO STORE
      this.store.dispatch(new USERLOGIN(response));
      this.status = 3;
    }, err => {
      this.status = 3;
      this.messageService.sendError('Userdaten konnten nicht geladen werden', err);
      console.log(err);
    });
  }

}
