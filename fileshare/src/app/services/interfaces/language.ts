export interface Language {
    language: string;
    initiated: boolean;
}
export const initialLanguage: Language = {
    language: 'de',
    initiated: false
};
