import { Languages } from './languages';

export interface TextBlock {
    title: Languages;
    desc: Languages;
}
