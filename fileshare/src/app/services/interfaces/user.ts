export interface User {
    admin: boolean;
    developer: boolean;
    data: any;
    email: string;
    name: any;
    online: boolean;
}
export const initialUser: User = {
    admin: false,
    developer: false,
    data: {
        jwt: {},
        azure: []
    },
    email: 'john.doe@eon.com',
    name: {
        firstName: 'John',
        lastName: 'Doe'
    },
    online: true
};
