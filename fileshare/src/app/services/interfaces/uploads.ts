export interface Uploads {
    files: Array<any>;
    initiated: boolean;
}

export const initialUploads: Uploads = {
    files: [],
    initiated: false
};
