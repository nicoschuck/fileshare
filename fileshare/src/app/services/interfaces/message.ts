export interface Message {
    txt: string;
    type: string;
    icon: string;
    dur: number;
}
